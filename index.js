require('dotenv').config();
const express = require("express");
const mongoose = require("mongoose");

const app = require('./src/api/index.js')

const PORT = process.env.PORT;

const uri = process.env.DB_URL;

async function connectDB(){
   mongoose.connect(uri, {useNewUrlParser: true});
}

app.listen(process.env.PORT || PORT, async function(){
  await connectDB();
  console.log(`Server started at port ${PORT}`);
});

process.on('unhandledRejection', error => {
  console.log('unhandledRejection', error);
});

process.on('uncaughtException', function (err) {
  console.error(err);
});
