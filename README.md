# To Install Dependencies ``` npm install ```

# To start the server ``` npm start ```

# Add a .env file which should contain:
```
PORT - localhost port number
DB_URL - MongoDB url
PVT_KEY - secret key to generate token
MAIL_SERVICE - mail service url
PHOTO_UPLOAD_SERVICE - photo upload service url
```

# Routes

## User Routes
### GET ```/users/auth/:token```
```
User have to activate his/her account with activation link sent to his/her email during signup.
```


### GET ```/users/```
```
Gets the details of the users. One can filter users using query paramters such as limit, email, username
```

### GET ```/users/:id```
```
Get the profile details of the user with the given id.
```

### GET ```/users/resetpassword```
```
User need to be logged in to reset password and a reset password link will be sent to user's email.
```

### POST ```/users/signup```
```
Request body should contain a JSON object of:
{
    "username" : your_username,
    "password" : your_password,
    "email" : your_email,
}

An activation link with 1 hr expiry will be sent to user's email upon successful request.
```

### POST ```/users/login```
```
Request body should contain a JSON object of:
{
    "username" : your_username,
    "password" : your_password
}

An Authentication token is returned upon successful request.
```

### POST ```/users/forgot_password```
```
Request body should contain a JSON object of:
{
    "email" : your_email,
}
A reset password link with 1 hr expiry will be sent to user's email upon successful request.
```

### POST ```/users/forgotpassword/:token```
```
{
    "password" : your new password,
    "confirmpassword" : enter the password again
}
```

### POST ```/users/resetpassword/:token```
```
{
    "password" : your new password,
    "confirmpassword" : enter the password again
}
```

### POST ```/users/:id```
```
User requires authorization to update details. The body of this request can contain one/more of the following: 
{
    "public" : boolean
}
```

### POST ```/users/profilepic```
```
User requires authorization to add profile picture. 
Form-data
{
    "file" : //attach file here
}
```

## Friend Routes

### GET ```/users/getfriends```
```
User requires authorization to get all his/her friends list.
```

### GET ```/users/getallfriendrequests```
```
User requires authorization to get all his/her friend requests sent.
```

### GET ```/users/getpendingrequests```
```
User requires authorization to get all his/her pending friend requests.
```

### GET ```/users/removefriend/:fid```
```
User requires authorization to remove a friend from his/her friends list with id fid.
```

### GET ```/users/sendfriendrequest/:fid```
```
User requires authorization to send a friend request to a friend with id fid.
```

### GET ```/users/acceptfriendrequest/:fid```
```
User requires authorization to accept a friend request with id fid.
```

### GET ```/users/rejectfriendrequest/:fid```
```
User requires authorization to reject a friend request received from a friend with id fid.
```

## Post Routes

### GET ```/users/posts/```
```
User requires authorization to get all his posts.
```

### GET ```/users/posts/feed```
```
User requires authorization to get all posts of all public users.
```

### GET ```/users/posts/:pid```
```
User requires authorization to get a public user's post details with post id pid.
```

### GET ```/users/posts/share/:pid```
```
User requires authorization to share a post with id pid.
```

### GET ```/users/posts/like/:pid```
```
User requires authorization to like a post with id pid.
```

### GET ```/users/posts/unlike/:pid```
```
User requires authorization to unlike a post with id pid.
```

### POST ```/users/posts/```
```
User requires authorization to create post. The body of this should be
{
    "file" : //Attach Image file here,
    "caption" : "Your caption here"
}
```

### PUT ```/users/posts/:pid```
```
User requires authorization to update his own post with id pid. The body of this should be
{
    "caption" : "Your updated caption here"
}
```

### DELETE ```/users/posts/:pid```
```
User requires authorization to delete his own post with id pid.
```

## Comment Routes

### GET ```/users/posts/comments/allcomments/:pid```
```
User requires authorization to get all comments under a post with id pid.
```
### GET ```/users/posts/comments/:cid```
```
User requires authorization to get a particular comment with id cid.
```

### POST ```/users/posts/comments/:pid```
```
User requires authorization to create a comment under a post with id pid. The body of this request should be
{
    "content" : "Your comment here"
}
```
### POST ```/users/posts/comments/reply/:cid```
```
User requires authorization to reply a comment of id cid.
{
    "content" : "Your reply here"
}
```
### PUT ```/users/posts/comments/:cid```
```
User requires authorization to update his own comment. The body of this request should be
{
    "content" : "Your updated comment here"
}
```
### DELETE ```/users/posts/comments/:cid```
```
User requires authorization to delete his own comment of if cid.
```