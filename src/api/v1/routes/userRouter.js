const express = require("express");
const app = express.Router()
const controller = require("../controllers/user")
const verifyToken = require("../middlewares/index.js");
const postRouter = require("./postRouter.js");
const multer = require("multer")();

app.use("/posts", verifyToken, postRouter);

app.post("/signup", controller.signup);

app.post('/login', controller.login);

app.get("/", controller.getAllUsers);

app.get("/resetpassword", verifyToken, controller.sendResetPasswordLink);
app.post("/forgot_password", controller.forgotPassword);

app.post("/profilepic", verifyToken, multer.single("file"), controller.profilepic);

app.get("/getfriends", verifyToken, controller.getallfriends);
app.get("/getallfriendrequests", verifyToken, controller.getallrequests);
app.get("/getpendingrequests", verifyToken, controller.getpendingrequests);

app.get("/removefriend/:fid", verifyToken, controller.removefriend); 
app.get("/sendfriendrequest/:fid", verifyToken, controller.sendfriendrequest);
app.get("/acceptfriendrequest/:fid", verifyToken, controller.acceptfriendrequest);
app.get("/rejectfriendrequest/:fid", verifyToken, controller.rejectrequest);

app.get("/:id", controller.profile);
app.post("/:id", verifyToken, controller.updateProfile);

app.get("/auth/:token", controller.activateAccount);

app.post("/forgotpassword/:token", controller.resetPassword);

app.post("/resetpassword/:token", controller.resetPassword);


module.exports = app;