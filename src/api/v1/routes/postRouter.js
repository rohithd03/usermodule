const express = require("express");
const app = express.Router();
const controller = require("../controllers/post");
const commentRouter = require("./commentRouter.js");
const verifyToken = require("../middlewares/index.js");
const multer = require("multer")();

app.use("/comments", verifyToken, commentRouter);

app.get("/ping", controller.ping);

app.get("/feed", controller.getfeed);

app.get("/", controller.getallposts);

app.post("/", multer.single("file") ,controller.createpost);
app.get("/:pid", controller.getpost);
app.put("/:pid", controller.updatepost);
app.delete("/:pid", controller.deletepost);

app.get("/share/:pid", controller.sharepost);

app.get("/like/:pid", controller.setlike);
app.get("/unlike/:pid", controller.setunlike);



module.exports = app;