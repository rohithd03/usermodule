const express = require("express");
const app = express.Router();
const controller = require("../controllers/comment.js");
const verifyToken = require("../middlewares/index.js");


app.get("/ping", controller.ping);

app.get("/allcomments/:pid", controller.getallcomments);

app.post("/:pid", controller.createcomment);
app.get("/:cid", controller.getcomment);
app.put("/:cid", controller.updatecomment);
app.delete("/:cid", controller.deletecomment);

app.post("/reply/:cid", controller.createreply);

module.exports = app;