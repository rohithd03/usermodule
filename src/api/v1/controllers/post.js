const services = require('../services/userServices');
const Post = require("../models/post");
const axios = require('axios');
const FormData = require('form-data');


module.exports.ping = async (req, res, next) => {
    try {
        return res.status(200).send({"Success": true, "message": "Working."})
    } catch (error) {
        next(error);
    }
}

module.exports.createpost = async (req, res, next) => {
    try {

        if(req.file){
            let formData = new FormData();
            formData.append('file', req.file.buffer, req.file.originalname);

            var response = await axios.post(process.env.PHOTO_UPLOAD_SERVICE, 
                formData,{
                    headers: {
                        'Content-Type': `multipart/form-data; boundary=${formData._boundary}`
                    }
                }
            );

            const result = await services.findOneDocument(0, {username: req.username});

            let post = new Post({
                user: result.id,
                image: response.data.url,
                caption: req.body.caption
            });

            await post.save()

            result.posts.push(post.id);
            await result.save();

            return res.status(200).send(post);
        }

        return res.status(400).send({"message": "Image File required."})

        
    } catch (error) {
        next(error);
    }
}

module.exports.updatepost = async (req, res, next) => {
    try {
        const posts = await services.findOneDocument(1, {"_id": req.params.pid});

        if(!posts){
            return res.status(400).send({"message": "Post does not exist."});
        }

        await services.findDocumentAndUpdate(1, {"_id": req.params.pid}, {caption: req.body.caption});

        return res.status(200).send({"Success": true});
    } catch (error) {
        next(error)
    }
}

module.exports.getpost = async (req, res, next) => {
    try {
        const result = await services.findById(1, req.params.pid);
        if(result){
            return res.status(200).send(result);
        }
        
        return res.status(400).send({"message": "Post does not exist."})
    } catch (error) {
        next(error);
    }
}

module.exports.deletepost = async (req, res, next) => {
    try {

        const posts = await services.findOneDocument(1, {"_id": req.params.pid});

        if(!posts){
            return res.status(400).send({"message": "Post does not exist."});
        }

        await services.deleteone(1, req.params.pid);
        return res.status(200).send({"Success": true, "message": "Post has been deleted."});
    } catch (error) {
        next(error);
    }
}

module.exports.getallposts = async (req, res, next) => {
    try {
        const result = await services.findOneDocument(0, {username: req.username});
        const allposts = await services.findDocuments(1, {user: result.id});

        return res.status(200).send(allposts);
    } catch (error) {
        next(error);
    }
}

module.exports.sharepost = async (req, res, next) => {
    try {
        const result = await services.findOneDocument(0, {username: req.username});
        const posts = await services.findOneDocument(1, {"_id": req.params.pid});

        if(!posts){
            return res.status(400).send({"message": "Post does not exist."});
        }

        let post = new Post({
            user: result.id,
            image: posts.image,
            caption: posts.caption,
            sharedfrom: posts.user 
        });

        await post.save();
        
        const shareduser = await services.findOneDocument(0, {id: posts.user});

        shareduser.posts.push(post.id);
        await shareduser.save();

        return res.status(200).send(post);

    } catch (error) {
        next(error);
    }
}

module.exports.setlike = async (req, res, next) => {
    try {
        const result = await services.findOneDocument(0, {username: req.username});
        const post = await services.findOneDocument(1, {"_id": req.params.pid});

        if(!post){
            return res.status(400).send({"message": "Post does not exist."});
        }

        post.liked.push(result.id);
        await post.save();

        result.liked_posts.push(post.id);
        await result.save();

        return res.status(200).send({"Success": true, "message": "Liked this post successfully."})
    } catch (error) {
        next(error);
    }
}

module.exports.setunlike = async (req, res, next) => {
    try {
        const result = await services.findOneDocument(0, {username: req.username});
        const post = await services.findOneDocument(1, {"_id": req.params.pid});

        if(!post){
            return res.status(400).send({"message": "Post does not exist."});
        }

        post.liked.pull(result.id);
        await post.save();

        result.liked_posts.pull(post.id);
        await result.save();

        return res.status(200).send({"Success": true, "message": "Un-Liked this post successfully."})
    } catch (error) {
        next(error);
    }
}

module.exports.getfeed = async (req, res, next) => {
    try {
        const feed = await services.findDocuments(1, {});
        return res.status(200).send(feed);
    } catch (error) {
        next(error);
    }
}