const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const services = require('../services/userServices');
const User = require("../models/user");
const Post = require("../models/post");
const Comment = require("../models/comment");
const axios = require('axios');
const FormData = require('form-data');
const { ServerCapabilities } = require('mongodb');


module.exports.ping = async (req, res, next) => {
    try {
        return res.status(200).send({"Success": true, "message": "Working."})
    } catch (error) {
        next(error);
    }
}

module.exports.createcomment = async (req, res, next) => {
    try {
        const result = await services.findOneDocument(0, {username: req.username});
        const post = await services.findOneDocument(1, {"_id": req.params.pid});

        if(!post){
            return res.status(400).send({"message": "Post does not exist."});
        }

        const author = await services.findOneDocument(0, {"_id": post.user});
        if(!author.public){
            return res.status(400).send({"message": "It's a private user's post."})
        }

        let comment = new Comment({
            user: result.id,
            post: req.params.pid,
            content: req.body.content
        });

        await comment.save()

        post.comments.push(comment.id);
        await post.save();

        result.comments.push(comment.id);
        await result.save();

        return res.status(200).send(comment);
    } catch (error) {
        next(error);
    }
}

module.exports.updatecomment = async (req, res, next) => {
    try {

        const comment = await services.findOneDocument(1, {"_id": req.params.cid});

        if(!comment){
            return res.status(400).send({"message": "Comment does not exist."});
        }

        await services.findDocumentAndUpdate(2, {"_id": req.params.cid}, {content: req.body.content});

        return res.status(200).send({"Success": true});
    } catch (error) {
        next(error)
    }
}

module.exports.getcomment = async (req, res, next) => {
    try {
        const result = await services.findById(2, req.params.cid);
        return res.status(200).send(result);
    } catch (error) {
        next(error);
    }
}

module.exports.deletecomment = async (req, res, next) => {
    try {
        const comment = await services.findById(2, req.params.cid);

        if(!comment){
            return res.status(400).send({"message": "Comment does not exist."});
        }
        
        const result = await services.findOneDocument(0, {"_id": comment.user});
        result.comments.pull(req.params.cid);
        const post = await services.findOneDocument(1, {id: comment.post});
        post.comments.pull(req.params.cid);

        await result.save();
        await post.save();
        const deleted = await services.deleteone(2, req.params.cid);
        return res.status(200).send({"Success": true, "Message": "Comment has been deleted.", "ack": deleted});
    } catch (error) {
        next(error);
    }
}

module.exports.getallcomments = async (req, res, next) => {
    try {

        const post = await services.findOneDocument(1, {"_id": req.params.pid});

        if(!post){
            return res.status(400).send({"message": "Post does not exist."});
        }

        const allcomments = await services.populate(1, req.params.pid, "comments", "content");

        return res.status(200).send(allcomments.comments);
    } catch (error) {
        next(error);
    }
}

module.exports.createreply = async (req, res, next) => {
    try {
        const result = await services.findOneDocument(0, {username: req.username});
        const comments = await services.findById(2, req.params.cid);

        if(!comments){
            return res.status(400).send({"message": "Comment does not exist."});
        }

        let comment = new Comment({
            user: result.id,
            post: comments.post,
            content: req.body.content
        });

        await comment.save()

        comments.reply.push(comment.id);
        await comments.save();
        
        result.comments.push(comment.id);
        await result.save();

        return res.status(200).send(comment);
    } catch (error) {
        next(error)
    }
}