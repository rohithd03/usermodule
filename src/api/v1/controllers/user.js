const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const services = require('../services/userServices');
const User = require("../models/user");
const axios = require('axios');
const FormData = require('form-data');

module.exports.signup = async (req, res, next) => {
    try {
        if(req.body.password && req.body.username && req.body.email){
            var salt = bcrypt.genSaltSync(10);
            var hashnew = bcrypt.hashSync(req.body.password, salt);

            let user = {
                username: req.body.username,
                hash: hashnew,
                email: req.body.email
            };

            const token = jwt.sign({user: user, type: "activate"}, process.env.PVT_KEY, { expiresIn: '1h' });

            var url = "http://localhost:3000/users/auth/" + token;

            await axios.post(process.env.MAIL_SERVICE, {
                "to": user.email,
                "subject": "Activation Link",
                "email_body": url
            })

            return res.status(200).send({"message": "Activate your account using link sent to your email."});
        }

        var err = new Error("Required Email, Username and password!");
        err.status = 400;
        throw err;
        
    } catch (error) {
        next(error);
    } 

}

module.exports.activateAccount = async (req, res, next) => {
    try {
        const details = jwt.verify(req.params.token, process.env.PVT_KEY);
        console.log("details", details)

        details.user.email_verified = true;

        let user = new User(details.user);

        await user.save();

        return res.send(user);
    } catch (error) {
        next(error);
    }
}

module.exports.login = async (req, res, next) => {
    try {
        if(req.body.username && req.body.password){
            const result = await services.findOneDocument(0, {username: req.body.username});
            console.log(result);
            if(bcrypt.compareSync(req.body.password, result.hash)){
                const token = jwt.sign({username: result.username, email: result.email, type: "login"}, process.env.PVT_KEY, { expiresIn: '1d' });
                return res.status(200).send({"message": "Login successful, use token as Bearer token.","token": token})
            }
            var err = new Error("Incorrect Credentials.");
            err.status = 400;
            throw err;
        }
        var err = new Error("Required Username and password!");
        err.status = 400;
        throw err;
    } catch (error) {
        next(error);
    }
}

module.exports.profile = async (req, res, next) => {
    try {
        const result = await services.findOneDocument(0, {"_id": req.params.id});
        if(result){
            if(result.public){
                return res.status(200).send(result);
            }
            var err = new Error("It's a private user.")
            err.status = 400;
            throw err;
        }
        var err = new Error("Invalid User ID.")
        err.status = 400;
        throw err;
    } catch (error) {
        next(error);
    }
}

module.exports.getAllUsers = async (req, res, next) => {
    try {

        const query = {};
        if(req.query.username){
            query.username = req.query.username;
        }
        else if(req.query.email) {
            query.email = req.query.email;
        }

        const limit = req.query.limit ? req.query.limit : 100;
        const options = { sort: { 'username': 'asc' }, limit: parseInt(limit)};
        console.log(options);

        var result = await services.findDocuments(0, query, options);
        result = result.map((user) => {
            return {id: user.id, username: user.username};
        })

        return res.status(200).send(result);
    } catch (error) {
        next(error);
    }
}

module.exports.updateProfile = async (req, res, next) => {
    try {
        const result = await services.findOneDocument(0, {username: req.username});
        if(result.id == req.params.id){
            const public = req.body.public || result.public;
            await services.findDocumentAndUpdate(0, {username: req.username}, {public: public});
            return res.status(200).send({"success": true, "message": "Successfully updated profile."});
        }
        var err = new Error("You're not Authorized");
        err.status = 401;
        throw err;
    } catch (error) {
        next(error);
    }
}

module.exports.forgotPassword = async (req, res, next) => {
    try {
        let user = {
            "email": req.body.email
        }
        const token = jwt.sign({user: user, type: "forgotPassword"}, process.env.PVT_KEY, { expiresIn: '1h' });

        var url = "http://localhost:3000/users/forgotpassword/" + token;

        await axios.post(process.env.MAIL_SERVICE, {
            "to": user.email,
            "subject": "Link to Reset Password",
            "email_body": url
        })
        return res.status(200).send({"message": "Reset your account password using the link sent to your email."});
    } catch (error) {
        next(error)
    }
}

module.exports.sendResetPasswordLink = async (req, res, next) => {
    try {
        let user = {
            "email": req.email
        }
        const token = jwt.sign({user: user, type: "resetPassword"}, process.env.PVT_KEY, { expiresIn: '1h' });

        var url = "http://localhost:3000/users/resetpassword/" + token;

        await axios.post(process.env.MAIL_SERVICE, {
            "to": user.email,
            "subject": "Link to Reset Password",
            "email_body": url
        })
        return res.status(200).send({"message": "Reset your account password using the link sent to your email."});
    } catch (error) {
        next(error);
    }
}

module.exports.resetPassword = async (req, res, next) => {
    try {
        if(req.body.password == req.body.confirmpassword){
            var salt = bcrypt.genSaltSync(10);
            var hashnew = bcrypt.hashSync(req.body.password, salt);

            const details = jwt.verify(req.params.token, process.env.PVT_KEY);
            await services.findDocumentAndUpdate(0, {email: details.user.email}, {hash: hashnew});

            return res.status(200).send({"success": true, "message": "Successfully updated password."});
        }
        var err = new Error("passwords does not match.");
        err.status = 401;
        throw err;
    } catch (error) {
        next(error);
    }
}

module.exports.sendfriendrequest = async (req, res, next) => {
    try {
        const result = await services.findOneDocument(0, {username: req.username});

        const friend = await services.findOneDocument(0, {"_id": req.params.fid});

        if(!friend){
            return res.status(400).send({"message": "User does not exist."});
        }

        if(result.friends.includes(req.params.fid)){
            return res.status(400).send({"message": "You're already friends."});
        }

        if(result.received_requests.includes(req.params.fid)){
            return res.status(400).send({"message": "This request has already sent to you."})
        }

        if(!result.sent_requests.includes(req.params.fid)){

            result.sent_requests.push(req.params.fid);
            await result.save();

            const friend = await services.findById(0, req.params.fid);
            friend.received_requests.push(result.id);
            await friend.save();
        
            return res.status(200).send({"success": true, "message": "Friend request sent successfully."})
        }
        
        return res.status(400).send({"message": "Request has already been sent."})
        
    } catch (error) {
        next(error);
    }
}

module.exports.acceptfriendrequest = async (req, res, next) => {
    try {
        const result = await services.findOneDocument(0, {username: req.username});

        const friend = await services.findOneDocument(0, {"_id": req.params.fid});

        if(!friend){
            return res.status(400).send({"message": "User does not exist."});
        }

        if(result.friends.includes(req.params.fid)){
            return res.status(200).send({"message": "You're already friends."});
        }

        if(result.received_requests.includes(req.params.fid)){

            result.received_requests.pull(req.params.fid);
            result.friends.push(req.params.fid);
            await result.save();

            const friend = await services.findById(0, req.params.fid);
            friend.sent_requests.pull(result.id);
            friend.friends.push(result.id);
            await friend.save();
        
            return res.status(200).send({"success": true, "message": "You are friends now."})
        }
        
        return res.status(200).send({"message": "You have no such request to accept."})
    } catch (error) {
        next(error);
    }
}

module.exports.getallfriends = async (req, res, next) => {
    try {

        const result = await services.findOneDocument(0, {username: req.username});
        const allfriends = await services.populate(0, result.id, "friends", "username");

        return res.status(200).send(allfriends.friends)
    } catch (error) {
        next(error);
    }
}

module.exports.getallrequests = async (req, res, next) => {
    try {
        const result = await services.findOneDocument(0, {username: req.username});
        const allrequests = await services.populate(0, result.id, "received_requests", "username");

        return res.status(200).send(allrequests.received_requests)
    } catch (error) {
        next(error);
    }
}

module.exports.removefriend = async (req, res, next) => {
    try {
        const result = await services.findOneDocument(0, {username: req.username});

        const friend = await services.findOneDocument(0, {"_id": req.params.fid});

        if(!friend){
            return res.status(400).send({"message": "User does not exist."});
        }

        if(result.friends.includes(req.params.fid)){

            result.friends.pull(req.params.fid);
            await result.save();

            const friend = await services.findById(0, req.params.fid);
            friend.friends.pull(result.id);
            await friend.save();
        
            return res.status(200).send({"success": true, "message": "You are not friends now."})
        }
        
        return res.status(400).send({"message": "You are not friends."})
    } catch (error) {
        next(error);
    }
}

module.exports.getpendingrequests = async (req, res, next) => {
    try {
        const result = await services.findOneDocument(0, {username: req.username});
        const allrequests = await services.populate(0, result.id, "sent_requests", "username");

        return res.status(200).send(allrequests.sent_requests);
    } catch (error) {
        next(error);
    }
}

module.exports.rejectrequest = async (req, res, next) => {
    try {
        const result = await services.findOneDocument(0, {username: req.username});

        const friend = await services.findOneDocument(0, {"_id": req.params.fid});

        if(!friend){
            return res.status(400).send({"message": "User does not exist."});
        }

        if(result.received_requests.includes(req.params.fid)){

            result.received_requests.pull(req.params.fid);
            await result.save();

            const friend = await services.findById(0, req.params.fid);
            friend.sent_requests.pull(result.id);
            await friend.save();
        
            return res.status(200).send({"success": true, "message": "Request rejected successfully."})
        }
        
        return res.status(400).send({"message": "You have no friend request to reject."})
    } catch (error) {
        next(error);
    }
}

module.exports.profilepic = async (req, res, next) => {
    try {

        if(req.file){
            let formData = new FormData();
            formData.append('file', req.file.buffer, req.file.originalname);

            var response = await axios.post(process.env.PHOTO_UPLOAD_SERVICE,  
                formData,{
                    headers: {
                        'Content-Type': `multipart/form-data; boundary=${formData._boundary}`
                    }
                }
            )
            
            const result = await services.findOneDocument(0, {username: req.username});

            result.profilepic = response.data.url;
            await result.save();

            return res.status(200).send(result);
        }

        return res.status(400).send({"message": "File is required."});
    } catch (error) {
        next(error);
    }
}