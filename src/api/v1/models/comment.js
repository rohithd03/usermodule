const mongoose = require("mongoose");
const { Schema } = mongoose;

const commentSchema = new mongoose.Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    post: {
        type: Schema.Types.ObjectId,
        ref: 'Post',
        required: true
    },
    content: {
        type: String,
        required: true
    },
    reply: [{
        type: Schema.Types.ObjectId,
        ref: 'Comment'
    }]
}, {timestamps: {createdAt: "created_at", updatedAt: "updated_at"}})

module.exports = mongoose.model("Comment", commentSchema);