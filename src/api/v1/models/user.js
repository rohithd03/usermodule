const mongoose = require("mongoose");
const { Schema } = mongoose;

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    hash: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    email_verified: {
        type: Boolean,
        default: false
    },
    profilepic: {
        type: String
    },
    public: {
        type: Boolean,
        default: false
    },
    friends: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],
    received_requests: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],
    sent_requests: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],
    posts: [{
        type: Schema.Types.ObjectId,
        ref: 'Post'
    }],
    liked_posts: [{
        type: Schema.Types.ObjectId,
        ref: 'Post'
    }],
    comments: [{
        type: Schema.Types.ObjectId,
        ref: 'Comment'
    }]
}, {timestamps: {createdAt: "created_at", updatedAt: "updated_at"}})

module.exports = mongoose.model("User", userSchema);