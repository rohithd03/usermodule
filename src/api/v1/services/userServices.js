const User = require("../models/user.js");
const Post = require("../models/post.js");
const Comment = require("../models/comment.js");

const schemas = [User, Post, Comment];

module.exports.findOneDocument = async (i, query) => {
    try {
        const result = await schemas[i].findOne(query)
        return result;
    } catch (error) {
        return error;
    }
}

module.exports.findDocuments = async (i, query, options={}) => {
    try {
        const result = await schemas[i].find(query, null, options)
        return result;
    } catch (error) {
        return error;
    }
}

module.exports.findDocumentAndUpdate = async (i, query, values) => {
    try {
        const result = await schemas[i].findOneAndUpdate(query, values)
        return result;
    } catch (error) {
        return error;
    }
}

module.exports.findById = async (i, value) => {
    try {
        const result = await schemas[i].findById(value);
        return result;
    } catch (error) {
        return error;
    }
}

module.exports.populate = async (i, value, array, select) => {
    try {
        const result = await schemas[i].findById(value).populate({path: array, select: select});
        return result;
    } catch (error) {
        return error;   
    }
}

module.exports.deleteone = async (i, value) => {
    try {
        const result = await schemas[i].deleteOne({"_id": value});
        return result;
    } catch (error) {
        return error;
    }
}