const jwt = require('jsonwebtoken');

async function verifyToken(req, res, next){
    try {
        console.log(req.params)
        const token = req.headers.authorization.split(" ")[1];
        const details = jwt.verify(token, process.env.PVT_KEY);
        console.log("details", details)
        if(details.type == "login"){
            req.username = details.username;
            req.email = details.email;
            next()
        }
        else if(details.type == "resetpassword"){
            req.email = details.email;
            next();
        }
        else
            throw new Error();
        
    } catch (error) {
        console.log("error")
        var err = new Error("Invalid Token");
        err.status = 403;
        next(err);
    }
}

module.exports = verifyToken;